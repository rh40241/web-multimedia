class SceneMenu extends Phaser.Scene {
	constructor() {
		super("SceneMenu");
	}
	preload() {
		this.load.image(
			"background",
			"assets/8-bit-weapon-bits-with-byte-background.png"
		);
		this.load.image("Button", "assets/StartButton.png");

		this.load.audio("music", ["assets/Music1.mp3", "assets/Music1.mp3"]);

		
		
	}
	create() {
		this.background = this.add.tileSprite(
			0,
			0,
			config.width,
			config.height,
			"background"
		);
		this.background.setOrigin(0, 0);

		this.Button = this.add.image(295, 260, "Button");
		this.Button.setOrigin(0, 0);
		this.input.on("pointerdown", () => this.scene.start("SceneMain"));
		this.Button.setScale(0.3);

		this.music = this.sound.add("music");

		var musicConfig = {
			mute: false,
			volume: 1,
			rate: 1,
			detune: 0,
			seek: 0,
			loop: false,
			delay: 0
		}
		

		
	}
	update() {
		this.background.tilePositionY -= 0.5;
		this.music.stop();
	}
}
