class SceneGameOver extends Phaser.Scene {
	constructor() {
		super("SceneGameOver");
	}
	preload() {
		this.load.image("gameOver", "assets/GameoverSMB.png");
	}
	create() {
		this.gameOver = this.add.image(50, 70, "gameOver");
		this.gameOver.setOrigin(0, 0);
		this.gameOver.setScale(2);

		this.text = this.add.text(225, 400, "Press x to play again", {
			color: "#ffffff",
			fontSize: "25px",
		});

		this.input.keyboard.on(
			"keyup_X",
			function (event) {
				this.scene.start("SceneMenu");
			},
			this
		);

		// this.text = this.add.text(16, 16, "Your score: " + this.scoreText, {
		// 	color: "#ffffff",
		// 	fontSize: "25px",
		// });
	}
}
