class SceneMain extends Phaser.Scene {
	constructor() {
		super("SceneMain");
	}
	preload() {
		this.load.image(
			"background",
			"assets/8-bit-weapon-bits-with-byte-background.png"
		);

		this.load.spritesheet("ship1", "assets/ship1.png", {
			frameWidth: 16,
			frameHeight: 16,
		});
		this.load.spritesheet("ship2", "assets/ship2.png", {
			frameWidth: 32,
			frameHeight: 16,
		});
		this.load.spritesheet("ship3", "assets/ship3.png", {
			frameWidth: 32,
			frameHeight: 32,
		});

		this.load.spritesheet("explosion", "assets/explosion.png", {
			frameWidth: 16,
			frameHeight: 16,
		});

		this.load.audio("audio_explosion", ["assets/explosion.mp3", "assets/explosion.mp3"]);

		this.load.audio("audio_GameOver", ["assets/GameOver.mp3", "assets/GameOver.mp3"]);

		this.load.audio("music", ["assets/Music1.mp3", "assets/Music1.mp3"]);
		
	}
	create() {
		this.physics.world.setBoundsCollision();

		this.background = this.add.tileSprite(
			0,
			0,
			config.width,
			config.height,
			"background"
		);
		this.background.setOrigin(0, 0);

		this.anims.create({
			key: "ship1_anim",
			frames: this.anims.generateFrameNumbers("ship1"),
			frameRate: 20,
			repeat: 1,
		});
		this.anims.create({
			key: "ship2_anim",
			frames: this.anims.generateFrameNumbers("ship2"),
			frameRate: 20,
			repeat: -1,
		});
		this.anims.create({
			key: "ship3_anim",
			frames: this.anims.generateFrameNumbers("ship3"),
			frameRate: 20,
			repeat: -1,
		});

		this.ship1 = this.add.sprite(200, 0, "ship1");
		this.ship2 = this.add.sprite(400, 0, "ship2");
		this.ship3 = this.add.sprite(600, 0, "ship3");

		this.ship1.setScale(2);
		this.ship2.setScale(2);
		this.ship3.setScale(2);

		this.ship1.play("ship1_anim");
		this.ship2.play("ship2_anim");
		this.ship3.play("ship3_anim");

		this.ship1.setInteractive();
		this.ship2.setInteractive();
		this.ship3.setInteractive();

		this.anims.create({
			key: "explode",
			frames: this.anims.generateFrameNumbers("explosion"),
			frameRate: 20,
			repeat: 0,
			hideOnComplete: false,
		});

		this.input.on("gameobjectdown", this.destroyShip, this);

		this.score = 0;

		this.scoreText = this.add.text(16, 16, "Score: 0", {
			color: "#ffffff",
			fontSize: "32px",
		});

		this.ExplosionSound = this.sound.add("audio_explosion");
		this.GameOverSound = this.sound.add("audio_GameOver");

		this.music = this.sound.add("music");

		var musicConfig = {
			mute: false,
			volume: 1,
			rate: 1,
			detune: 0,
			seek: 0,
			loop: false,
			delay: 0
		}
		this.music.play(musicConfig);
	}

	update() {
		this.background.tilePositionY -= 0.5;

		this.moveShip(this.ship1, 1);
		this.moveShip(this.ship2, 2);
		this.moveShip(this.ship3, 3);
	}
	destroyShip(pointer, gameObject) {
		gameObject.setTexture("explosion");
		gameObject.play("explode");

		this.ExplosionSound.play();
		


		setTimeout(() => {
			gameObject.setTexture("ship" + Phaser.Math.Between(1, 3));
			this.resetShipPos(gameObject);
		}, 300);

		this.score += 10;
		this.scoreText.setText("Score: " + this.score);
	}

	moveShip(ship, speed) {
		ship.y += speed;
		if (ship.y === config.height) {
			
			this.scene.start("SceneGameOver");
			this.GameOverSound.play();
			this.ExplosionSound.stop();
			this.music.stop();
			
		}
	}
	resetShipPos(ship) {
		ship.y = 0;
		ship.x = Phaser.Math.Between(0, config.width);
	}
}
