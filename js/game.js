var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
	scene: [SceneMenu, SceneMain, SceneGameOver],
	physics: {
		default: "arcade",
		arcade: {
			debug: false,
	
		},
	},
};

	var game = new Phaser.Game(config);

	var musicConfig = {
		mute: false,
		volume: 1,
		rate: 1,
		detune: 0,
		seek: 0,
		loop: false,
		delay: 0
	}



