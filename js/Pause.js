class ScenePause extends Phaser.Scene {
	constructor() {
		super("ScenePause");
	}
	preload() {
		this.load.image("Pause", "assets/pause.png");
	}
	create() {
		this.Pause = this.add.image(50, 70, "Pause");
		this.Pause.setOrigin(0, 0);
		this.Pause.setScale(2);

		this.text = this.add.text(225, 400, "Press p to resume", {
			color: "#ffffff",
			fontSize: "25px",
		});

		this.input.keyboard.on(
			"keyup_P",
			function (event) {
				this.scene.resume("SceneMain");
			},
			this
		);

		// this.text = this.add.text(16, 16, "Your score: " + this.scoreText, {
		// 	color: "#ffffff",
		// 	fontSize: "25px",
		// });
	}
}